<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class ShareControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get Company search
     *
     * @return void
     */
    public function test_share_companies()
    {
        $response = $this->post('/api/share/companies');

        $response->assertStatus(500);

        $user = User::factory()->create();

        $response = $this
                        ->actingAs($user)
                        ->postJson('/api/share/companies');

        $response
                ->assertStatus(200)
                ->assertJson([
                    'success' => false,
                    'message' => 'The search field is required.',
                    'data' => []
                ]);

        $response = $this
                        ->actingAs($user)
                        ->postJson('/api/share/companies', ['code' => 'KO.XLIM']);

        $response->assertStatus(200);

    }

}
