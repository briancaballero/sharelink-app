<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\CompanyData;

class CompanyDataTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * Test Data insert
     *
     * @return void
     */
    public function test_company_data_insert()
    {
        $cdata = CompanyData::factory()->create();

        $this->assertDatabaseHas('company_data', array(
            'symbol' => $cdata->symbol
        ));
    }
}
