<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Events\FetchedCompanyEOD;
use Carbon\Carbon;
use App\Models\CompanyData;

class ShareController extends Controller
{
    static private $mshost;
    static private $mskey;

    /**
     * Set MarketStack host and key
     */
    public function __construct()
    {
        self::$mshost = env('MARKETSTACK_ACCESS_HOST');
        self::$mskey = env('MARKETSTACK_ACCESS_KEY');
    }

    /**
     * Get list of companies
     * MS API: tickers
     */
    public function companies(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'required',
        ]);

        $data = [];

        if ($validator->fails()) {
            $success = false;
            $message = $validator->errors()->first();
        } else {
            $response = Http::get(self::$mshost . 'tickers', [
                'access_key' => self::$mskey,
                'search' => $request->search
            ]);

            if ($response->successful()) {
                $tickers = json_decode($response->body());
                if(!empty($tickers->data)) {
                    $success = true;
                    $message = 'success';
                    $data = $tickers->data;
                } else {
                    $success = false;
                    $message = 'No result for "' . $request->search . '"';
                }
                
            } else {
                $success = false;
                $message = $response->body();
            }
        }

        $response = [
            'success' => $success,
            'message' => $message,
            'data' => $data
        ];

        return response()->json($response);
    }

    /**
     * Get list of companies
     * MS API: /tickers/[symbol]/eod
     */
    public function current(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        $data = [];

        if ($validator->fails()) {
            $success = false;
            $message = $validator->errors()->first();
        } else {

            // check/use if there is already data on company_data table

            if($cdata = CompanyData::where(['symbol' => $request->code])->get()->first()) {
                $response = [
                    'success' => true,
                    'message' => 'success from table',
                    'data' => json_decode($cdata->data)
                ];
        
                return response()->json($response);
            }

            $response = Http::get(self::$mshost . 'tickers/' . $request->code . '/eod', [
                'access_key' => self::$mskey
            ]);

            if ($response->successful()) {
                $success = true;
                $message = 'success';
                $tickers = json_decode($response->body());
                
                if($tickers->data->has_eod) {
                    $data = [
                        'companyname' => $tickers->data->name,
                        'currenteod_date' => $tickers->data->eod[0]->date,
                        'currenteod_open' => $tickers->data->eod[0]->open,
                        'currenteod_close' => $tickers->data->eod[0]->close,
                        'currenteod_high' => $tickers->data->eod[0]->high,
                        'currenteod_low' => $tickers->data->eod[0]->low,
                        'currenteod_volume' => $tickers->data->eod[0]->volume,
                        'currenteod_adjopen' => $tickers->data->eod[0]->adj_open,
                        'currenteod_adjclose' => $tickers->data->eod[0]->adj_close,
                        'currenteod_adjhigh' => $tickers->data->eod[0]->adj_high,
                        'currenteod_adjlow' => $tickers->data->eod[0]->adj_low,
                        'currenteod_adjvolume' => $tickers->data->eod[0]->adj_volume,
                        'currenteod_split_factor' => $tickers->data->eod[0]->split_factor,
                        'currenteod_dividend' => $tickers->data->eod[0]->dividend
                    ];

                    event(new FetchedCompanyEOD($request->code, Carbon::parse($tickers->data->eod[0]->date)->format('Y-m-d'), $data));

                } else {
                    $success = false;
                    $message = 'No current data. Please select other company.';
                }

                
                
            } else {
                $success = false;
                $message = $response->body();
            }
        }

        $response = [
            'success' => $success,
            'message' => $message,
            'data' => $data
        ];

        return response()->json($response);
    }
}
