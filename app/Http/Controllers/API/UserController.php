<?php

namespace App\Http\Controllers\API;

use Session;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Register
     */
    public function register(Request $request)
    {
        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            $success = true;
            $message = 'User register successfully';
        } catch (\Illuminate\Database\QueryException $ex) {
            $success = false;
            $message = $ex->getMessage();
        }

        // response
        $response = [
            'success' => $success,
            'message' => $message,
        ];
        return response()->json($response);
    }

    /**
     * Login
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($credentials)) {
            $success = true;
            $message = 'User login successfully!';
        } else {
            $success = false;
            $message = 'Unauthorised!';
        }

        // response
        $response = [
            'success' => $success,
            'message' => $message,
        ];
        return response()->json($response);
    }

    /**
     * Logout
     */
    public function logout()
    {
        try {
            Session::flush();
            $success = true;
            $message = 'Successfully logged out';
        } catch (\Illuminate\Database\QueryException $ex) {
            $success = false;
            $message = $ex->getMessage();
        }

        // response
        $response = [
            'success' => $success,
            'message' => $message,
        ];
        return response()->json($response);
    }

    /*
     * CRUD Users
     */

    // all users
    public function index()
    {
        $users = User::all()->toArray();
        return array_reverse($users);
    }

    // add user
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:40|min:3',
            'email' => 'required|email:rfc,dns',
            'password' => 'required|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            $success = false;
            $message = $validator->errors()->first();
        } else {
            try {
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->save();
    
                $success = true;
                $message = 'User register successfully';
            } catch (\Illuminate\Database\QueryException $ex) {
                $success = false;
                $message = $ex->getMessage();
            }        
        }

        // response
        $response = [
            'success' => $success,
            'message' => $message,
        ];

        return response()->json($response);
        
    }

    // edit user
    public function edit($id)
    {
        $response = [
            'success' => false
        ];

        try {
            $user = User::find($id);
            $response['success'] = true;
            $response['user'] = $user;
        } catch (\Illuminate\Database\QueryException $ex) {
            $response['message'] = $ex->getMessage();
        }

        return response()->json($response);
    }

    // update user
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:40|min:3',
            'email' => 'required|email:rfc,dns',
            'password' => 'sometimes|nullable|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            $success = false;
            $message = $validator->errors()->first();
        } else {
            try {
                $user = User::find($id);
                $user->name = $request->name;
                $user->email = $request->email;
                if(!empty($request->password)) {
                    $user->password = Hash::make($request->password);
                }
                $user->save();
                $success = true;
                $message = 'The user successfully updated';
            } catch (\Illuminate\Database\QueryException $ex) {
                $success = false;
                $message = $ex->getMessage();
            }
        }

        // response
        $response = [
            'success' => $success,
            'message' => $message,
        ];

        return response()->json($response);
    }

    // delete user
    public function delete($id)
    {
        try {
            $user = User::find($id);
            $user->delete();
            $success = true;
            $message = 'The user successfully deleted';
        } catch (\Illuminate\Database\QueryException $ex) {
            $success = false;
            $message = $ex->getMessage();
        }

        // response
        $response = [
            'success' => $success,
            'message' => $message,
        ];

        return response()->json($response);
    }
}