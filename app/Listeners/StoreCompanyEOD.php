<?php

namespace App\Listeners;

use App\Events\FetchedCompanyEOD;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\CompanyData;
use Carbon\Carbon;

class StoreCompanyEOD
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\FetchedCompanyEOD  $event
     * @return void
     */
    public function handle(FetchedCompanyEOD $event)
    {
        // make sure that record has the current/latest data
        CompanyData::firstOrCreate([
                'symbol' => $event->symbol,
                'eod_date' => $event->eod_date
            ],
            [
                'data' => json_encode($event->data)
            ]);
    }
}
