<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FetchedCompanyEOD
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $symbol;
    public $eod_date;
    public $data;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($symbol, $eod_date, $data)
    {
        $this->symbol = $symbol;
        $this->eod_date = $eod_date;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
