<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\PurgeCompanyDataJob;

class DispatchJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch All Jobs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        PurgeCompanyDataJob::dispatch();
        return 0;
    }
}
