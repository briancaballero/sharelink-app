import {createApp} from 'vue'
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

require('./bootstrap');
var moment = require('moment');
import App from './layouts/App.vue';
import axios from 'axios';
import router from './router';

import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';

// import SimpleTypeahead from 'vue3-simple-typeahead';
// import 'vue3-simple-typeahead/dist/vue3-simple-typeahead.css'; //Optional default CSS

const app = createApp(App)
app.config.globalProperties.$axios = axios;
app.config.globalProperties.moment = moment;
app.use(router);
// app.use(SimpleTypeahead);
app.use(VueLoading);
// app.use(BootstrapVue);
// app.use(IconsPlugin);
app.mount('#app');