<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class CompanyDataFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'symbol' => $this->faker->text(8),
            'eod_date' => Carbon::now(),
            'data' => $this->faker->text(500)
        ];
    }
}
